﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Constants;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using static Otus.Teaching.PromoCodeFactory.WebHost.Extensions.MapExtension;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IRepository<Partner> _partnersRepository;

        public PartnersController(IRepository<Partner> partnersRepository)
        {
            _partnersRepository = partnersRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
        {
            var partners = await _partnersRepository.GetAllAsync();

            var response = partners.Select(x => new PartnerResponse
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits.Select(y => y.MapToResponse()).ToList()
            });

            return Ok(response);
        }
        
        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimit>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
            {
                return NotFound(ErrorMessage.PartnerNotFound);
            }

            var limit = partner.PartnerLimits.FirstOrDefault(x => x.Id == limitId);

            var response = limit.MapToResponse();

            return Ok(response);
        }
        
        /// <summary>
        /// Установка лимита
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            #region Проверки

            if (partner == null)
            {
                return NotFound(ErrorMessage.PartnerNotFound);
            }

            if (!partner.IsActive)
            {
                return BadRequest(ErrorMessage.PartnerIsInactive);
            }

            if (request.Limit <= 0)
            {
                return BadRequest(ErrorMessage.LimitMustBeGreaterThanZero);
            }

            #endregion
            
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                // Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал
                // Если лимит закончился, то количество не обнуляется
                if (activeLimit.EndDate >= DateTime.Today)
                {
                    partner.NumberIssuedPromoCodes = 0;
                }

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }
            
            // Установка лимита партнеру

            var newLimit = new PartnerPromoCodeLimit
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };
            
            partner.PartnerLimits.Add(newLimit);
            
            await _partnersRepository.UpdateAsync(partner);
            
            return CreatedAtAction(nameof(GetPartnerLimitAsync), new {id = partner.Id, limitId = newLimit.Id}, null);
        }
        
        /// <summary>
        /// Отключение лимита
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            #region Проверки

            if (partner == null)
            {
                return NotFound(ErrorMessage.PartnerNotFound);
            }

            if (!partner.IsActive)
            {
                return BadRequest(ErrorMessage.PartnerIsInactive);
            }

            #endregion

            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
                await _partnersRepository.UpdateAsync(partner);
            }

            return NoContent();
        }

    }
}