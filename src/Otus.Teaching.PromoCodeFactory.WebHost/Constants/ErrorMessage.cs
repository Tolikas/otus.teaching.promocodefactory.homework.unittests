﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Constants
{
    public static class ErrorMessage
    {
        public const string PartnerNotFound = "Данный партнёр не найден";

        public const string PartnerIsInactive = "Данный партнёр не активен";

        public const string LimitMustBeGreaterThanZero = "Лимит должен быть больше 0";
    }
}
