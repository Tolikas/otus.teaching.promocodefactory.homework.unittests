﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factories
{
    public static class PartnerFactory
    {
        public static Partner CreateBasePartner()
        {
            var partnerPromoCodeLimit = PartnerPromoCodeLimitFactory.CreateBasePartnerPromoCodeLimit();
            var partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>
            {
                partnerPromoCodeLimit
            };

            return new PartnerBuilder()
                .WithId(Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"))
                .WithName("Суперигрушки")
                .WithActive(true)
                .WithNumberIssuedPromoCodes(0)
                .WithPartnerLimits(partnerPromoCodeLimits)
                .Build();
        }

        public static Partner WithActive(this Partner partner, bool isActive)
        {
            partner.IsActive = isActive;
            return partner;
        }

        public static Partner WithNumberIssuedPromoCodes(this Partner partner, int numberIssuedPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;
            return partner;
        }
    }
}