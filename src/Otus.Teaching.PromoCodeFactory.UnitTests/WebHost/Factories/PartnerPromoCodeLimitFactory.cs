﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factories
{
    public static class PartnerPromoCodeLimitFactory
    {
        public static PartnerPromoCodeLimit CreateBasePartnerPromoCodeLimit()
        {
            return new PartnerPromoCodeLimitBuilder()
                .WithId(Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"))
                .WithCreateDate(new DateTime(2020, 07, 9))
                .WithEndDate(new DateTime(2020, 10, 9))
                .WithLimit(100)
                .Build();
        }

        public static PartnerPromoCodeLimit WithNumberIssuedPromoCodes(this PartnerPromoCodeLimit limit, DateTime endDate)
        {
            limit.EndDate = endDate;
            return limit;
        }

    }
}
