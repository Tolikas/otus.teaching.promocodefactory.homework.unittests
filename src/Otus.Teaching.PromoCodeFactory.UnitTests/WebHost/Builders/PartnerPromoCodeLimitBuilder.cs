﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders
{
    public class PartnerPromoCodeLimitBuilder
    {
        private Guid _id;
        private DateTime _createDate;
        private DateTime? _cancelDate;
        private DateTime _endDate;
        private int _limit;

        public PartnerPromoCodeLimitBuilder()
        {
        }

        public PartnerPromoCodeLimitBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithCreateDate(DateTime createDate)
        {
            _createDate = createDate;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithCancelDate(DateTime? cancelDate)
        {
            _cancelDate = cancelDate;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit
            {
                Id = _id,
                CreateDate = _createDate,
                CancelDate = _cancelDate,
                EndDate = _endDate,
                Limit = _limit
            };
        }

    }
}