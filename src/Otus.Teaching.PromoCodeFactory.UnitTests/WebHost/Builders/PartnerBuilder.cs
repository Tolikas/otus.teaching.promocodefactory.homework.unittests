﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders
{
    public class PartnerBuilder
    {
        private Guid _id;
        private string _name;
        private bool _isActive;
        private int _numberIssuedPromoCodes;
        private ICollection<PartnerPromoCodeLimit> _partnerLimits;

        public PartnerBuilder()
        {
        }

        public PartnerBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public PartnerBuilder WithActive(bool isActive)
        {
            _isActive = isActive;
            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _numberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }

        public PartnerBuilder WithPartnerLimits(ICollection<PartnerPromoCodeLimit> partnerLimits)
        {
            _partnerLimits = partnerLimits;
            return this;
        }

        public Partner Build()
        {
            return new Partner
            {
                Id = _id,
                Name = _name,
                IsActive = _isActive,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes,
                PartnerLimits = _partnerLimits
            };
        }

    }
}