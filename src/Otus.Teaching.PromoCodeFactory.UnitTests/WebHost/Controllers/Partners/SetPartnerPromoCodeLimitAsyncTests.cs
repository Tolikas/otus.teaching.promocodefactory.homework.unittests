﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using FluentAssertions;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Constants;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        
        private void MockPartnerGetByIdAsync(Guid partnerId, Partner partner)
        {
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
        }
        
        public SetPartnerPromoCodeLimitRequest CreateLimitRequest(int? limit = null)
        {
            var defaultLimit = 10;

            var limitRequest = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Today.AddMonths(1),
                Limit = limit ?? defaultLimit
            };

            return limitRequest;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            MockPartnerGetByIdAsync(partnerId, partner);

            var limitRequest = CreateLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            // Assert
            result.Should().BeEquivalentTo(new NotFoundObjectResult(ErrorMessage.PartnerNotFound));
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerFactory.CreateBasePartner()
                .WithActive(false);

            MockPartnerGetByIdAsync(partnerId, partner);

            var limitRequest = CreateLimitRequest();
            
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            // Assert
            result.Should().BeEquivalentTo(new BadRequestObjectResult(ErrorMessage.PartnerIsInactive));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsLessOrEqualToZero_ReturnsBadRequest(int limit)
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerFactory.CreateBasePartner();

            MockPartnerGetByIdAsync(partnerId, partner);

            var limitRequest = CreateLimitRequest(limit);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            // Assert
            result.Should().BeEquivalentTo(new BadRequestObjectResult(ErrorMessage.LimitMustBeGreaterThanZero));
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitMustBeCancelled_ReturnsTrue()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerFactory.CreateBasePartner();

            MockPartnerGetByIdAsync(partnerId, partner);

            var limitRequest = CreateLimitRequest();

            var limitBefore = partner.PartnerLimits.FirstOrDefault(p => !p.CancelDate.HasValue);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);
            var result = partner.PartnerLimits.FirstOrDefault(p => p.Id == limitBefore.Id).CancelDate;

            // Assert
            result.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitHasNotExpired_NumberIssuedPromoCodesIsZeroed()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerFactory.CreateBasePartner()
                .WithNumberIssuedPromoCodes(10);

            MockPartnerGetByIdAsync(partnerId, partner);

            var limitRequest = CreateLimitRequest();

            var limitBefore = partner.PartnerLimits.FirstOrDefault(p => !p.CancelDate.HasValue);
            limitBefore.EndDate = DateTime.Today.AddMonths(1);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            var result = partner.NumberIssuedPromoCodes;

            // Assert
            result.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitHasExpired_NumberIssuedPromoCodesNotChanged()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerFactory.CreateBasePartner()
                .WithNumberIssuedPromoCodes(10);

            MockPartnerGetByIdAsync(partnerId, partner);

            var limitRequest = CreateLimitRequest();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            var result = partner.NumberIssuedPromoCodes;

            // Assert
            result.Should().Be(10);
        }

    }
}